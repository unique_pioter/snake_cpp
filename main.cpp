#include <iostream>
#include <thread>
#include <chrono>

#include <SFML/Graphics.hpp>
#include <SFML/Window/Event.hpp>

#include "GameMap.hpp"
#include "Snake.hpp"

sf::RectangleShape createBox()
{
	sf::RectangleShape rectangle(sf::Vector2f(60.f, 60.f));
	rectangle.setFillColor(sf::Color(0, 255, 0));
	rectangle.setPosition(10.f, 10.f);
	return rectangle;
}

void timer(Snake& snake)
{
	while(snake.isAlive())
	{
		// snake.tick();
		std::this_thread::sleep_for(std::chrono::milliseconds(2000000));
	}
}

int main()
{
	sf::RenderWindow window(sf::VideoMode(800, 800), "Snake window");
	
	GameMap gm{10, 10};
	Snake snake{1};
	snake.initializeSnake();

	std::thread tickTimerThread(timer, std::ref(snake));

	auto mapTiles = gm.prepareInitialMap(80.f);

	// sf::ConvexShape testConvex{8};
	// testConvex.setFillColor(sf::Color(255,0,0));
	// testConvex.setPosition(sf::Vector2f{400.f, 400.f});
	// testConvex.setPoint(0, sf::Vector2f(20.f, 0.f));
	// testConvex.setPoint(1, sf::Vector2f(60.f, 0.f));
	// testConvex.setPoint(2, sf::Vector2f(60.f, 20.f));
	// testConvex.setPoint(3, sf::Vector2f(80.f, 20.f));
	// testConvex.setPoint(4, sf::Vector2f(80.f, 60.f));
	// testConvex.setPoint(5, sf::Vector2f(60.f, 60.f));
	// testConvex.setPoint(6, sf::Vector2f(20.f, 60.f));
	// testConvex.setPoint(7, sf::Vector2f(20.f, 20.f));
	// testConvex.setOrigin(sf::Vector2f{testConvex.getLocalBounds().width / 2, testConvex.getLocalBounds().height / 2});

	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			switch (event.type) {
				case sf::Event::Closed:
					window.close();
					break;
				case sf::Event::KeyPressed:
					switch (event.key.code){
						case sf::Keyboard::Key::Up:
							std::cout << " Up " << std::endl;
							snake.setHeading(Snake::Heading::UP);
							snake.tick();
							// testConvex.setRotation(90.f);
							// std::cout << "ConvexPosition" << testConvex.getPosition().x << " : " << testConvex.getPosition().y << "\n"; 
							break;
						case sf::Keyboard::Key::Down:
							std::cout << " Down " << std::endl;
							snake.setHeading(Snake::Heading::DOWN);
							snake.tick();
							// testConvex.setRotation(0);
							// std::cout << "ConvexPosition" << testConvex.getPosition().x << " : " << testConvex.getPosition().y << "\n"; 
							break;
						case sf::Keyboard::Key::Left:
							std::cout << " Left " << std::endl;
							snake.setHeading(Snake::Heading::LEFT);
							snake.tick();
							// testConvex.setRotation(180.f);
			
							// std::cout << "ConvexPosition" << testConvex.getPosition().x << " : " << testConvex.getPosition().y << "\n"; 
							break;
						case sf::Keyboard::Key::Right:
							std::cout << " Right " << std::endl;
							snake.setHeading(Snake::Heading::RIGHT);
							snake.tick();
							// testConvex.setRotation(270.f);
							// std::cout << "ConvexPosition" << testConvex.getPosition().x << " : " << testConvex.getPosition().y << "\n"; 
							break;
						}
					break;
				default:
					break;
			}
		}

		window.clear();
		for (auto & t : mapTiles)
		{
			//todo duplicate
			auto gB = t.getGlobalBounds();
			sf::RectangleShape boundingBox{sf::Vector2f{gB.width, gB.height}};
			boundingBox.setPosition(sf::Vector2f{ gB.left, gB.top});
			boundingBox.setOutlineThickness(2.f);
			boundingBox.setOutlineColor(sf::Color(0, 0, 0));
		
			window.draw(boundingBox);
			window.draw(t);
		}
		
		// testConvex.setOrigin(sf::Vector2f{0.f, 0.f});
		// window.draw(testConvex);

		snake.checkColisionWithTail();

		for (auto & snakeTile : snake.getSnakeBoxes())
		{
			//todo duplicate
			auto gB = snakeTile.getGlobalBounds();
			sf::RectangleShape boundingBox{sf::Vector2f{gB.width, gB.height}};
			boundingBox.setPosition(sf::Vector2f{ gB.left, gB.top});
			boundingBox.setFillColor(sf::Color{255,0,0,128});
			boundingBox.setOutlineThickness(5.f);
			boundingBox.setOutlineColor(sf::Color(255, 0, 0));
			window.draw(boundingBox);

			window.draw(snakeTile);
		}	

		if (!snake.isAlive())
		{
			break;
		}

		window.display();
	}

	tickTimerThread.join();

	return 0;
}
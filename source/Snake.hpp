#include <SFML/Graphics.hpp>
#include <cstdint>
#include <vector>

class Snake
{
	public:
		enum class Heading
		{
			UP,
			DOWN,
			LEFT,
			RIGHT
		};


	private: 
		uint8_t _velocity;
		uint8_t _size; 
		std::vector<sf::ConvexShape> snakeBoxes;
		std::vector<Snake::Heading> snakeBoxesHeadings;
		bool _isAlive;


	public:
		Snake(uint8_t velocity);
		void setHeading(Snake::Heading heading);
		uint8_t increaseVelocity();
		uint8_t increaseSize();
		void initializeSnake();
		std::vector<sf::ConvexShape> getSnakeBoxes(); 
		void tick(); //todo should be ITickable?
		void checkColisionWithTail();
		void calculateSnakeCorners();
		bool isAlive();
		void updateRotationBasedOnHeadings(sf::ConvexShape & shapeToRotate, const Heading heading);
		void setConvexAsLine(sf::ConvexShape& shape);
		void calculateSnakeCornersBasedOnHeadings();

};
#include "Snake.hpp"

#include <iostream>
#include <cmath>

Snake::Snake(uint8_t velocity):
 _velocity{velocity},
 _size{5},
 _isAlive{true}
{
	snakeBoxesHeadings.reserve(_size);
	snakeBoxesHeadings = std::vector<Snake::Heading>(_size, Snake::Heading::UP);
	snakeBoxes.reserve(_size);
}

uint8_t Snake::increaseVelocity()	
{
	return ++_velocity;
}

uint8_t Snake::increaseSize()
{
	return ++_size;
}

void Snake::setHeading(Heading heading)
{
	switch(heading)
	{
		case Snake::Heading::DOWN:
			if (snakeBoxesHeadings[0] == Snake::Heading::UP)
			{
				std::cout << "invalid move" << std::endl;
				return;
			}
			break;
		case Snake::Heading::UP:
			if (snakeBoxesHeadings[0] == Snake::Heading::DOWN)
			{
				std::cout << "invalid move" << std::endl;
				return;
			}
			break;
		case Snake::Heading::LEFT:
			if (snakeBoxesHeadings[0] == Snake::Heading::RIGHT)
			{
				std::cout << "invalid move" << std::endl;
				return;
			}
			break;
		case Snake::Heading::RIGHT:
			if (snakeBoxesHeadings[0] == Snake::Heading::LEFT)
			{
				std::cout << "invalid move" << std::endl;
				return;
			}
			break;
	}
	snakeBoxesHeadings[0] = heading;
}

void Snake::setConvexAsLine(sf::ConvexShape& shape)
{
	shape.setPoint(0, sf::Vector2f(20.f,0.f));
	shape.setPoint(1, sf::Vector2f(60.f,0.f));
	shape.setPoint(2, sf::Vector2f(60.f,20.f));
	shape.setPoint(3, sf::Vector2f(60.f,60.f));
	shape.setPoint(4, sf::Vector2f(60.f,80.f));
	shape.setPoint(5, sf::Vector2f(20.f,80.f));
	shape.setPoint(6, sf::Vector2f(20.f,60.f));
	shape.setPoint(7, sf::Vector2f(20.f,20.f));
}

void Snake::initializeSnake()
{
	sf::ConvexShape tileHead{3};
	tileHead.setPoint(0, sf::Vector2f(40.f,40.f));
	// tileHead.setPoint(1, sf::Vector2f(50.f,0.f));
	tileHead.setPoint(1, sf::Vector2f(70.f, 80.f));
	tileHead.setPoint(2, sf::Vector2f(10.f, 80.f));
	
	tileHead.setFillColor(sf::Color(0,255,0));
	tileHead.setPosition(4* 80.f, 4 * 80.f);
	snakeBoxes.push_back(tileHead);
	
	for (int i = 0; i < _size - 2; ++i)
	{
		sf::ConvexShape tileMiddle{8};
		setConvexAsLine(tileMiddle);
		
		tileMiddle.setFillColor(sf::Color(0,150,0));
		auto prevPostion = snakeBoxes[i].getPosition();
		tileMiddle.setPosition(prevPostion.x, prevPostion.y + 80.f);

		snakeBoxes.push_back(tileMiddle);
	}
	
	sf::ConvexShape tileTail{3};
	tileTail.setPoint(0, sf::Vector2f(20.f,0.f));
	tileTail.setPoint(1, sf::Vector2f(60.f,0.f));
	tileTail.setPoint(2, sf::Vector2f(40.f, 20.f));
	
	auto prevPostion = snakeBoxes.back().getPosition();
	tileTail.setPosition(prevPostion.x, prevPostion.y + 80.f);
	snakeBoxes.push_back(tileTail);
}

std::vector<sf::ConvexShape> Snake::getSnakeBoxes()
{
	return snakeBoxes;
}

void Snake::updateRotationBasedOnHeadings(sf::ConvexShape & shapeToRotate, const Heading heading)
{
		shapeToRotate.setOrigin(sf::Vector2f{shapeToRotate.getGlobalBounds().width / 2.f,
		 shapeToRotate.getLocalBounds().height / 2.f});
		switch(heading)
		{
			case Snake::Heading::DOWN:
				shapeToRotate.setRotation(180.f);
				break;
			case Snake::Heading::UP:
				shapeToRotate.setRotation(0.f);
				break;
			case Snake::Heading::LEFT:
				shapeToRotate.setRotation(270.f);
				break;
			case Snake::Heading::RIGHT:
				shapeToRotate.setRotation(90.f);
				break;
			}
		shapeToRotate.setOrigin(sf::Vector2f{0.f, 0.f});
}

void Snake::tick()
{
	if (_isAlive)
	{
		for (int i = snakeBoxes.size(); i >= 0; --i)
		{
			snakeBoxes[i + 1].setPosition(snakeBoxes[i].getPosition());
		}

		for (int i = snakeBoxesHeadings.size(); i >= 0; --i)
		{
			snakeBoxesHeadings[i + 1] = snakeBoxesHeadings[i];
		}
		
		const auto & head = snakeBoxes.front().getPosition();
		std::cout << "head position" << head.x << ":" << head.y << '\n';

		snakeBoxes.front().setOrigin(sf::Vector2f{snakeBoxes.front().getLocalBounds().width/2.f, snakeBoxes.front().getLocalBounds().height/2.f});
        switch(snakeBoxesHeadings.front())
        {
		    case Snake::Heading::DOWN:
		        snakeBoxes[0].move(0.f, 80.f);
				break;
            case Snake::Heading::UP:
                snakeBoxes[0].move(0.f, -80.f);
                break;
            case Snake::Heading::LEFT:
            	snakeBoxes[0].move(-80.f, 0.f);
                break;
            case Snake::Heading::RIGHT:
            	snakeBoxes[0].move(80.f, 0.f);
                break;
		}
		snakeBoxes.front().setOrigin(sf::Vector2f(0.f, 0.f));

// const auto & head = snakeBoxes.front().getPosition();
		std::cout << "head position" << head.x << ":" << head.y << '\n';

		updateRotationBasedOnHeadings(snakeBoxes.front(), snakeBoxesHeadings.front());
		updateRotationBasedOnHeadings(snakeBoxes.back(), snakeBoxesHeadings.back());

		checkColisionWithTail();
		calculateSnakeCornersBasedOnHeadings();
	
		// const auto & head = snakeBoxes.front().getPosition();
		std::cout << "head position" << head.x << ":" << head.y << '\n';
		 
	}
}

void Snake::calculateSnakeCorners()
{
	for (int i = 1; i < snakeBoxes.size() -1 ; ++i)
	{
		if( snakeBoxes[i].getPosition().x == snakeBoxes[i+1].getPosition().x 
			&& snakeBoxes[i].getPosition().y == snakeBoxes[i-1].getPosition().y)
			{
					// snakeBoxes[i].setFillColor(sf::Color(255,0,0));
					snakeBoxes[i].setPoint(0, sf::Vector2f(20.f, 0.f));
					snakeBoxes[i].setPoint(1, sf::Vector2f(60.f, 0.f));
					snakeBoxes[i].setPoint(2, sf::Vector2f(60.f, 20.f));
					snakeBoxes[i].setPoint(3, sf::Vector2f(80.f, 20.f));
					snakeBoxes[i].setPoint(4, sf::Vector2f(80.f, 60.f));
					snakeBoxes[i].setPoint(5, sf::Vector2f(60.f, 60.f));
					snakeBoxes[i].setPoint(6, sf::Vector2f(20.f, 60.f));
					snakeBoxes[i].setPoint(7, sf::Vector2f(20.f, 20.f));
				// korner prawo lewo
				if (snakeBoxes[i+1].getPosition().y > snakeBoxes[i-1].getPosition().y)
				{

					snakeBoxes[i].setRotation(90.f);
					snakeBoxes[i].setFillColor(sf::Color(255,255,255));
				}
				else
				{
					snakeBoxes[i].setRotation(270.f);
					snakeBoxes[i].setFillColor(sf::Color(255,0,255));
				}
			}
		else if ( snakeBoxes[i].getPosition().y == snakeBoxes[i+1].getPosition().y 
			&& snakeBoxes[i].getPosition().x == snakeBoxes[i-1].getPosition().x)
		{
				snakeBoxes[i].setFillColor(sf::Color(255,0,0));
				snakeBoxes[i].setPoint(0, sf::Vector2f(20.f, 0.f));
				snakeBoxes[i].setPoint(1, sf::Vector2f(60.f, 0.f));
				snakeBoxes[i].setPoint(2, sf::Vector2f(60.f, 20.f));
				snakeBoxes[i].setPoint(3, sf::Vector2f(60.f, 60.f));
				snakeBoxes[i].setPoint(4, sf::Vector2f(20.f, 60.f));
				snakeBoxes[i].setPoint(5, sf::Vector2f(0.f, 60.f));
				snakeBoxes[i].setPoint(6, sf::Vector2f(0.f, 20.f));
				snakeBoxes[i].setPoint(7, sf::Vector2f(20.f, 20.f));
				snakeBoxes[i].setFillColor(sf::Color(255,0,0));
			if (snakeBoxes[i+1].getPosition().x > snakeBoxes[i-1].getPosition().x)
			{
				snakeBoxes[i].setFillColor(sf::Color(0,255,255));
				snakeBoxes[i].setRotation(90.f);

			}
			else
			{
				snakeBoxes[i].setFillColor(sf::Color(0,0,255));
				snakeBoxes[i].setRotation(270.f);
			}
		}
		else
		{
			// nie jest kornerem
			setConvexAsLine(snakeBoxes[i]);
			snakeBoxes[i].setFillColor(sf::Color(0,255,0));
		}
	}
	//czy jestem kornerem??
}

void Snake::calculateSnakeCornersBasedOnHeadings()
{
	for (int i = 1; i < snakeBoxes.size() - 1; ++i)
	{
		if(snakeBoxesHeadings[i] == Heading::LEFT && snakeBoxesHeadings[i + 1 ] == Heading::UP ||
		snakeBoxesHeadings[i] == Heading::DOWN && snakeBoxesHeadings[i + 1 ] == Heading::RIGHT)
		{
			snakeBoxes[i].setPoint(0, sf::Vector2f(0.f, 20.f));
			snakeBoxes[i].setPoint(1, sf::Vector2f(40.f, 20.f));
			snakeBoxes[i].setPoint(2, sf::Vector2f(60.f, 20.f));
			snakeBoxes[i].setPoint(3, sf::Vector2f(60.f, 60.f));
			snakeBoxes[i].setPoint(4, sf::Vector2f(60.f, 80.f));
			snakeBoxes[i].setPoint(5, sf::Vector2f(20.f, 80.f));
			snakeBoxes[i].setPoint(6, sf::Vector2f(20.f, 60.f));
			snakeBoxes[i].setPoint(7, sf::Vector2f(0.f, 60.f));
			// snakeBoxes[i].setFillColor(sf::Color(255,0,0));

		}
		else if(snakeBoxesHeadings[i] == Heading::RIGHT && snakeBoxesHeadings[i + 1 ] == Heading::UP ||
		snakeBoxesHeadings[i] == Heading::DOWN && snakeBoxesHeadings[i + 1 ] == Heading::LEFT)
		{
			snakeBoxes[i].setPoint(0, sf::Vector2f(80.f, 20.f));
			snakeBoxes[i].setPoint(1, sf::Vector2f(80.f, 60.f));
			snakeBoxes[i].setPoint(2, sf::Vector2f(60.f, 60.f));
			snakeBoxes[i].setPoint(3, sf::Vector2f(60.f, 80.f));
			snakeBoxes[i].setPoint(4, sf::Vector2f(20.f, 80.f));
			snakeBoxes[i].setPoint(5, sf::Vector2f(20.f, 60.f));
			snakeBoxes[i].setPoint(6, sf::Vector2f(20.f, 20.f));
			snakeBoxes[i].setPoint(7, sf::Vector2f(40.f, 20.f));
			// snakeBoxes[i].setFillColor(sf::Color(255,0,0));
		}
		else if(snakeBoxesHeadings[i] == Heading::LEFT && snakeBoxesHeadings[i + 1 ] == Heading::DOWN ||
		snakeBoxesHeadings[i] == Heading::UP && snakeBoxesHeadings[i + 1 ] == Heading::RIGHT)
		{
			snakeBoxes[i].setPoint(0, sf::Vector2f(60.f, 0.f));
			snakeBoxes[i].setPoint(1, sf::Vector2f(60.f, 20.f));
			snakeBoxes[i].setPoint(2, sf::Vector2f(60.f, 60.f));
			snakeBoxes[i].setPoint(3, sf::Vector2f(20.f, 60.f));
			snakeBoxes[i].setPoint(4, sf::Vector2f(0.f, 60.f));
			snakeBoxes[i].setPoint(5, sf::Vector2f(0.f, 20.f));
			snakeBoxes[i].setPoint(6, sf::Vector2f(20.f, 20.f));
			snakeBoxes[i].setPoint(7, sf::Vector2f(20.f, 0.f));
			// snakeBoxes[i].setFillColor(sf::Color(255,0,0));
		}
		else if(snakeBoxesHeadings[i] == Heading::RIGHT && snakeBoxesHeadings[i + 1 ] == Heading::DOWN ||
		snakeBoxesHeadings[i] == Heading::UP && snakeBoxesHeadings[i + 1 ] == Heading::LEFT)
		{
			snakeBoxes[i].setPoint(0, sf::Vector2f(60.f, 0.f));
			snakeBoxes[i].setPoint(1, sf::Vector2f(60.f, 20.f));
			snakeBoxes[i].setPoint(2, sf::Vector2f(80.f, 20.f));
			snakeBoxes[i].setPoint(3, sf::Vector2f(80.f, 60.f));
			snakeBoxes[i].setPoint(4, sf::Vector2f(60.f, 60.f));
			snakeBoxes[i].setPoint(5, sf::Vector2f(20.f, 60.f));
			snakeBoxes[i].setPoint(6, sf::Vector2f(20.f, 20.f));
			snakeBoxes[i].setPoint(7, sf::Vector2f(20.f, 0.f));
			// snakeBoxes[i].setFillColor(sf::Color(255,0,0));
		}
		else
		{
			setConvexAsLine(snakeBoxes[i]);
			snakeBoxes[i].setFillColor(sf::Color(0,255,0));
			auto bounds = snakeBoxes[i].getLocalBounds();
			if(snakeBoxesHeadings[i] == Heading::LEFT || snakeBoxesHeadings[i] == Heading::RIGHT)
			{
				snakeBoxes[i].setOrigin(sf::Vector2f{bounds.width/2.f, bounds.height/2.f});
				snakeBoxes[i].setRotation(90.f);
			}
			else
			{
				snakeBoxes[i].setOrigin(sf::Vector2f{bounds.width/2.f, bounds.height/2.f});
				snakeBoxes[i].setRotation(0.f);
			}
			snakeBoxes[i].setOrigin(sf::Vector2f{0.f, 0.f});
		}
	
	}

}

void Snake::checkColisionWithTail()
{
	for (int i = 1; i < snakeBoxes.size(); ++i)
	{
		if (snakeBoxes[i].getPosition() == snakeBoxes.front().getPosition())
		{
			std::cout << "Auuuaa"  << i << std::endl;
			for(auto & i : snakeBoxes)
			{
				i.setFillColor(sf::Color(255,0,0));
			}
			_isAlive = false;
			return;
		}
	}
}

bool Snake::isAlive()
{
	return _isAlive;
}
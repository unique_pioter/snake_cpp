#include "GameMap.hpp"

#include <iostream>

GameMap::GameMap(uint8_t xSize, uint8_t ySize):
	_xSize{xSize},
	_ySize{ySize},
	tile1Color{100, 100, 100},
	tile2Color{50,50,50}
{

	loadTexture();

}

std::vector<sf::Sprite> GameMap::prepareInitialMap(float tileSize)
{
	std::vector<sf::Sprite> preparedMap;
	preparedMap.reserve(_xSize * _ySize);

	sf::Vector2f tileDimensions(tileSize, tileSize);

	for (auto i = 0; i < _xSize; ++i)
	{
		for (auto j = 0; j < _ySize; ++j)
		{
			sf::Sprite tile{};

			// if ((i + j) % 2 > 0)
			// {
			// 	tile.setFillColor(tile1Color);
			// }
			// else
			// {
			// 	tile.setFillColor(tile2Color);
			// }

			tile.setTexture(texture);
			tile.setTextureRect(sf::IntRect(18, 1, 41, 47));
			tile.setScale(80.f / 41.f, 80.f / 47.f);

			tile.setPosition(i * tileSize, j * tileSize);

			preparedMap.push_back(tile);
		}
	}

	return preparedMap;
}

void GameMap::loadTexture()
{
	if(texture.loadFromFile("/home/piotrek/Dev/snake_cpp/plains.png"))
		std::cout << "Good" << std::endl; 
}

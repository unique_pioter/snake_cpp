#include <cstdint>

#include <SFML/Graphics.hpp>

class GameMap
{
	public:
		GameMap(uint8_t xSize, uint8_t ySize);
		std::vector<sf::Sprite> prepareInitialMap(float tileSize);
		// should load some assets for map
		void loadTexture();

	private:
		uint8_t _xSize;
		uint8_t _ySize;

		sf::Color tile1Color;
		sf::Color tile2Color;

		sf::Texture texture;

};

